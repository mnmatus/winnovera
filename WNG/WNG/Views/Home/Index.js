﻿wngapp.controller('indexController', function ($scope, $http, $q, $window, $location) {        
    $scope.isBusy = false;
    $scope.listCard = [
          { url: '/images/redimage.jpg'}
        , { url: '/images/redimage.jpg'}
        , { url: '/images/redimage.jpg'}
        , { url: '/images/redimage.jpg'}
    ];   
    
    
    $scope.getCatImages = function () {
        $scope.isBusy = true;
        var numberOfImage = $scope.listCard.length;       
        $http({
            method: 'GET',            
            url: '/api/main/getimages/' + numberOfImage,
        }).then(function (response) {            
            for (var i = 0; i < numberOfImage; i++) {
                $scope.listCard[i].url = response.data[i];
            };
            $scope.isBusy = false;
        }).catch(function (response) {            
            alert('Unexpected error occured');
            $scope.isBusy = false;
        });        
    };
 
});