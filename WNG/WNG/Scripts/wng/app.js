﻿var wngapp = angular.module('wngapp', []);


wngapp.factory('authInterceptorService', ['$q', '$location', '$window', 
function ($q, $location, $window) {
    var authInterceptorServiceFactory = {};
    var _request = function (config) {
        config.headers = config.headers || {};       
        return config;
    }
    var _responseError = function (rejection) {
        debugger;
        if (rejection.status === 401) {
            alert('error 401');

        }
        return $q.reject(rejection);
    }
    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;
    return authInterceptorServiceFactory;
}]);




wngapp.config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptorService');
});




