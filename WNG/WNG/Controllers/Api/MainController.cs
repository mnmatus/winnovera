﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using System.Net.Http;
using System.Web.Http;

namespace WNG.Controllers.Api
{
    [RoutePrefix("api/main")]
    public class MainController : ApiController
    {
        [HttpGet]
        [Route("getimages/{numberOfImage}")]
        public IHttpActionResult GetImages(int numberOfImage)
        {           
            HttpResponseMessage response = null;
            var httpClient = new HttpClient();          
            httpClient.DefaultRequestHeaders.Add("Access-Control-Allow-Origin", "*");           
            response = httpClient.GetAsync("http://shibe.online/api/cats?count=" + numberOfImage + "&urls=true&httpsUrls=false").Result;
            var content = response.Content.ReadAsStringAsync().Result;            
            var result = JsonConvert.DeserializeObject<List<string>>(content);
            return Ok(result);
        }
    }
}
